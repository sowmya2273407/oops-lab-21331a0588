/*Acess modifier Private*/
#include<iostream>
using namespace std;
class Class1
{
  
    private:
    string name;
    public:
    void value(string n)
    {
      name=n;
      cout<<"Accessmodifier is "<<name<<endl;
    }
};
int main()
{
    Class1 obj;
    obj.value("Private");
}  