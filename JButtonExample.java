import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JButtonExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new FlowLayout());

        JButton button = new JButton();
        button.setBounds(50, 15, 120, 40);
        button.setText("Click me!");
        button.setBackground(Color.BLUE);
        button.setForeground(Color.WHITE);
        button.setEnabled(true);
        button.setToolTipText("This is a button");
        button.setFocusable(true);
        button.setFont(new Font("Arial", Font.BOLD, 14));
        button.setBorder(BorderFactory.createLineBorder(Color.PINK));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Button clicked!");
            }
        });

        frame.add(button);
        frame.setSize(200, 100);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
