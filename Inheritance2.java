class ElectronicDevice1
{
    ElectronicDevice1()
    {
        System.out.println("I am an electronic device.");
    }
}
class MusicPlayer extends ElectronicDevice
{
    MusicPlayer()
    {
        System.out.println("The music player has the option of forwarding/reversing the music");
    }
}
class hometheater extends MusicPlayer{
    hometheater()
    {
        System.out.println("It has a property of good bass");
    }
}
class MP3Player extends MusicPlayer
{
    MP3Player()
    {
        System.out.println("It has an option for FM radio");
    }
}
class Main{
    public static void main(String[] args) {   
   System.out.println("SINGLE INHERITANCE");
   MusicPlayer obj=new MusicPlayer();
   System.out.println("MULTILEVEL INHERITANCE");
    hometheater obj1=new hometheater();
     System.out.println("HIERARCHICAL Inheritance");
      hometheater obj3=new hometheater();
      MP3Player obj4=new MP3Player();
      System.out.println("this all inheritance is HYBRID INHERITANCE");
    }

}