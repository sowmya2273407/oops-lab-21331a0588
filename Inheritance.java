class ElectronicDevice
{
    ElectronicDevice()
    {
        System.out.println("I am an electronic device.");
    }
}
class Computer extends ElectronicDevice
{

    Computer()
    {
        System.out.println("I am a computer.");
    }
    public static void main(String[] args) {
        Computer obj=new Computer();
    }

}

