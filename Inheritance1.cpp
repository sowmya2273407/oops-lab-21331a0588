//SINGLE INHERITANCE
#include <iostream>
using namespace std;
class electronicDevice
{
public: 
    electronicDevice()
    {
        cout <<"I am an electronic device."<<endl;
    }
};
class Computer: public electronicDevice
{
public:
    Computer()
    {
        cout <<"I am a computer."<<endl;
    }

};
int main()
{
    Computer obj;
}