import java.util.*;
public class Collectionslist{
    public static void main(String[] args) {
        ArrayList<String> alist=new ArrayList<String>();
        alist.add("c");
        alist.add("C++");
        alist.add(1,"python");
        Iterator itr=alist.iterator();  
       while(itr.hasNext()){  
       System.out.println(itr.next());  
       } 
       System.out.println(alist.contains("python"));
        System.out.println(alist.indexOf("C++"));
        System.out.println(alist);
        System.out.println(alist.remove("c"));
        System.out.println(alist);
        System.out.println(alist.get(1));

        //Linked list
        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("Apple");
        linkedList.add("Banana");
        linkedList.set(0, "Cherry");
        System.out.println(linkedList);
        System.out.println(linkedList.get(1));
        System.out.println(linkedList.size());
        System.out.println(linkedList.remove(1));
        System.out.println(linkedList.remove("Orange"));
        System.out.println(linkedList.contains("Banana"));
        for (String fruit : linkedList) {
            System.out.println(fruit);
        }
        
        //vector methods same as arraylist
        Vector<String> alist1=new Vector<String>();
        alist1.add("carrot");
        alist1.add("Beetroot");
        alist1.add(1,"potato");
        Iterator itr1=alist1.iterator();  
       while(itr1.hasNext()){  
       System.out.println(itr1.next());  
       } 

       //Stack
      Stack<String> stack = new Stack<>();
      stack.push("Apple");
      stack.push("Banana");
      System.out.println(stack.peek());
      System.out.println(stack.pop());
      System.out.println(stack.isEmpty());
      System.out.println(stack.size());
      System.out.println(stack.search("Apple"));
      for (String element : stack) {
      System.out.println(element);
      }

    }
    
}