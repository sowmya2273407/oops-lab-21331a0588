/*Acess modifier Protected*/
#include<iostream>
using namespace std;
class Class1
{
    
    protected:
    string name;
    
};
class Class2:public Class1{
    public:
    void value(string n)
    {
      name=n;
      cout<<"Accessmodifier is "<<name<<endl;
    }
};
int main()
{
    Class2 obj;
    obj.value("Protected");
}  