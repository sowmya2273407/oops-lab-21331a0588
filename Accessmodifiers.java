class AccessSpecifierDemo
 {
     private int priVar;
     protected int proVar;
      public int pubVar;
      public void setVar(int priValue,int proValue, int pubValue){
        priVar=priValue;
        proVar= proValue;
         pubVar=pubValue;

      }
       public void getVar(){
        System.out.println("privalue is "+priVar);
        System.out.println("provalue is "+proVar);
        System.out.println("pubvalue is "+pubVar);

       }
       public static void main(String[] args) {
        AccessSpecifierDemo obj=new AccessSpecifierDemo();
        obj.setVar(10,20,30);
        obj.getVar();
       }
       
 }
