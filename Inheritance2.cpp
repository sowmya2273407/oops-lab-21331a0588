#include <iostream>
using namespace std;
class electronicDevice
{
public: 
    electronicDevice()
    {
        cout << "I am an electronic device."<<endl;
    }
};
class MusicPlayer:public electronicDevice
{
    public:
    MusicPlayer()
    {
        cout<<"The music player has the option of forwarding/reversing the music"<<endl;
    }
};
class hometheater:public MusicPlayer{
    public:
    hometheater()
    {
        cout<<"It has a property of good bass"<<endl;
    }
};
class MP3Player:public MusicPlayer
{
    public:
    MP3Player()
    {
        cout<<"It has an option for FM radio"<<endl;
    }
};
class Bluetooth: public hometheater,public MP3Player
{
    public:
    Bluetooth()
    {
        cout<<"It is a wireless technology"<<endl;
    }
};
int main()
{
   cout<<"SINGLE INHERITANCE"<<endl;
   MusicPlayer obj;
   cout<<"MULTILEVEL INHERITANCE"<<endl;
    hometheater obj1;
     cout<<"MULTiple INHERITANCE"<<endl;
     Bluetooth obj2;
      cout<<"HIERARCHICAL Inheritance"<<endl;
      hometheater obj3;
      MP3Player obj4;
       cout<<"all is HYBRID INHERITANCE"<<endl;
}