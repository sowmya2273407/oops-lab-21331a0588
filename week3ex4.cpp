#include<iostream>
using namespace std;
class Student
{
    private:
      string fullName;
     int rollNum;
    double semPercentage;
    string collegeName;
     int collegeCode;
 public:  
 Student();
 ~Student();
 void read();
 void disp();
};
Student :: Student()
{
 cout<<"This is the student details constructor called "<<endl;
}
void Student::read()
{
 cout<<"Enter the student full name "<<endl;
 cin>>fullName;
cout<<"Enter the student roll number "<<endl;
 cin>>rollNum;
 cout<<"Enter the student sem percentage "<<endl;
 cin>>semPercentage;
 cout<<"Enter the student college name "<<endl;
 cin>>collegeName;
 cout<<"Enter the student college code "<<endl;
 cin>>collegeCode;

}
void Student::disp()
{
    cout<<"student details"<<endl;
    cout<<"student name:"<<fullName<<endl;
    cout<<"student rollno:"<<rollNum<<endl;
    cout<<"student sempercentage:"<<semPercentage<<endl;
    cout<<"student collegename :"<<collegeName<<endl;
    cout<<"student collegecode :"<<collegeCode<<endl;
}
Student :: ~Student()
{
 cout<<"student details is closed "<<endl;
}
int main()
{
    Student s;
    s.read();
    s.disp();
}