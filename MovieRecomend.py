import numpy as np
import pandas as pd
import difflib #recomend movies based on matching to user input(close match)
from sklearn.feature_extraction.text import TfidfVectorizer #convert text data into numeric values
from sklearn.metrics.pairwise import cosine_similarity #similar movies we retreive

movies_data=pd.read_csv('movies.csv')

selected_features=['genres','keywords','tagline','cast','director']
#print(selected_features)

#Replacing the null values with null string
for feature in selected_features:
  movies_data[feature]=movies_data[feature].fillna('')

#combining all the 5 selected features

combined_features=movies_data['genres']+' '+movies_data['keywords']+' '+movies_data['tagline']+' '+movies_data['cast']+' '+movies_data['director']
#converting the text data to features vectors
vectorizer=TfidfVectorizer()
feature_vector=vectorizer.fit_transform(combined_features)

#getting the similarity scores using cosine similarity
similarity=cosine_similarity(feature_vector)

#movie_name=input(' Enter Your Favourate movie name : ')
from tkinter import *
def recomendmovie():
    txt.delete(0.0, 'end')
    movie = ent.get()
    list_of_all_titles = movies_data['title'].tolist()
    find_close_match = difflib.get_close_matches(movie, list_of_all_titles)
    
    if find_close_match:
        close_match = find_close_match[0]
        index_of_the_movie = movies_data[movies_data.title == close_match]['index'].values[0]
        similarity_score = list(enumerate(similarity[index_of_the_movie]))
        sorted_similar_movies = sorted(similarity_score, key=lambda x: x[1], reverse=True)

        i = 1
        list1 = [None] * 10
        for movie in sorted_similar_movies:
            index = movie[0]
            title_from_index = movies_data[movies_data.index == index]['title'].values[0]
            if i <= 10:
                list1[i - 1] = title_from_index
                i += 1

        for x in range(len(list1) - 1, -1, -1):
            t = "\n"
            txt.insert('end', t)
            txt.insert('end', list1[x])

      

root=Tk()
root.geometry("420x300")
root.title('MOVIE RECOMENDATION SYSTEM')              
l1 = Label(root, text="Enter Movie name: ")
l2 = Label(root, text="Top Ten Suggtion For You: ")
               
ent =Entry(root)
            
l1.grid(row=0)
l2.grid(row=2)
               
ent.grid(row=0, column=1)            
               
txt=Text(root,width=50,height=13, wrap=WORD)
txt.grid(row=3, columnspan=2, sticky=W)
               
btn=Button(root, text="Search", bg="purple", fg="white", command=recomendmovie)
btn.grid(row=1, columnspan=2)
root.mainloop()