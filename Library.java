/*    LIBRARY MANAGEMENT SYSTEM   */
import java.awt.*;
import java.awt.event.*;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import java.text.SimpleDateFormat; 
import java.util.Date; 

public class Library 
{
  static Label uname;
  static TextField uname1,add1,cname1,Mno1;
  static Font font;
    Library()
    {
      Frame fr=new Frame();
      fr.setLayout(null);
      fr.setVisible(true);
      fr.setTitle("LIBRARY");
      fr.setSize(700,700);
      Color formColor = new Color(53, 59, 72);
      fr.setBackground(formColor);

      Label title =new Label("LIBRARY MANAGEMENT SYSTEM");
      title.setForeground(Color.YELLOW);
      font= new Font("Courier", Font.BOLD, 20);
      title.setFont(font);
      title.setBounds(130,70, 330, 40);
      fr.add(title);
        
      Label uname=new Label("Enter your name: ");
      font= new Font("Courier", Font.TRUETYPE_FONT, 18);
      uname.setFont(font);
      uname.setForeground(Color.WHITE);
      uname.setBounds(130,150,150,40);
      fr.add(uname);

      uname1=new TextField();
      font= new Font("Courier", Font.PLAIN, 15);
      uname1.setFont(font);
      uname1.setBounds(430,150,150,40);
      fr.add(uname1);

      Label cname=new Label("Enter your college name: ");
      font= new Font("Courier", Font.TRUETYPE_FONT, 18);
      cname.setFont(font);
      cname.setForeground(Color.WHITE);
      cname.setBounds(130,210,200,40);
      fr.add(cname);

      cname1=new TextField();
      font= new Font("Courier", Font.PLAIN, 15);
      cname1.setFont(font);
      cname1.setBounds(430,210,150,40);
      fr.add(cname1);

      Label Mno=new Label("Enter your Registration number:");
      font= new Font("Courier", Font.TRUETYPE_FONT, 18);
      Mno.setFont(font);
      Mno.setForeground(Color.WHITE);
      Mno.setBounds(130,280,260,40);
      fr.add(Mno);

      Mno1=new TextField();
      font= new Font("Courier", Font.PLAIN, 15);
      Mno1.setFont(font);
      Mno1.setBounds(430,280,150,40);
      fr.add(Mno1);

      Button faculty=new Button(" Faculty ");
      font= new Font("Courier", Font.TRUETYPE_FONT, 20);
      faculty.setFont(font);
      faculty.setBounds(180,430,90,50);
      faculty.setBackground(Color.BLUE);
      faculty.setForeground(Color.WHITE);
      fr.add(faculty);
      faculty.addActionListener(
          new ActionListener() {
              public void actionPerformed(ActionEvent e) {  
                fr.dispose();
                String s=LMS2.uname1.getText();
                String a=LMS2.Mno1.getText();
                JOptionPane.showMessageDialog(fr,"hello "+ s +" Roll Number : "+a);
                choice obj=new choice();        
              }
            }
         );

      Button student=new Button(" Student ");
      font= new Font("Courier", Font.TRUETYPE_FONT, 20);
      student.setFont(font);
      student.setBounds(380,430,90,50);
      student.setBackground(Color.BLUE);
      student.setForeground(Color.WHITE);
      fr.add(student);
      student.addActionListener(
          new ActionListener() {
              public void actionPerformed(ActionEvent e) {  
                fr.dispose();
                String s=LMS2.uname1.getText();
                String a=LMS2.Mno1.getText();
                JOptionPane.showMessageDialog(fr,"hello "+ s +" Roll Number : "+a);
               choice obj2=new choice();        
              }
           }
         );

       
      fr.addWindowListener(
          new WindowAdapter() {
            public void windowClosing(WindowEvent e)
            {
              fr.dispose();
            }
          }
        );
    }

  public static void main(String[] args)
  {
    LMS2 obj1=new LMS2();
  }
}

//Click the chioce that you want.

class choice 
{
  Label msg;
  Font font;
  choice() 
  {

    Frame f=new Frame();

    Label choice1=new Label("   Click your Choice ");
    font= new Font("Courier", Font.CENTER_BASELINE, 20);
    choice1.setFont(font);
    choice1.setBounds(120,80,200,40);
    choice1.setBackground(Color.WHITE);
    f.add(choice1);

    Button b1=new Button("Request for Book");
    font= new Font("Courier", Font.CENTER_BASELINE, 20);
    b1.setFont(font);
    b1.setBackground(Color.ORANGE);
    b1.setBounds(40,180,185,50);
    f.add(b1);
    
    b1.addActionListener(
      new ActionListener() {
       public void actionPerformed(ActionEvent e)
       {
          f.dispose();
          Request R=new Request();
        }
      }
    );

    Button b2=new Button("Book Return");
    font= new Font("Courier", Font.CENTER_BASELINE, 18);
    b2.setFont(font);
    b2.setBackground(Color.CYAN);
    b2.setBounds(260,180,165,50);
    f.add(b2);
    b2.addActionListener(
      new ActionListener() {
        public void actionPerformed(ActionEvent e)
        {
          f.dispose();
          Return R=new Return();
        }
      }
    );

    Button b3=new Button("View Books");
    font= new Font("Courier", Font.CENTER_BASELINE, 18);
    b3.setFont(font);
    b3.setBackground(Color.PINK);
    b3.setBounds(40,280,185,50);
    f.add(b3);

    b3.addActionListener(
      new ActionListener() {
        public void actionPerformed(ActionEvent e)
        {
          f.dispose();
          viewBooks vb=new viewBooks();
        }
      }
    );

    Button b4=new Button(" EXIT ");
    font= new Font("Courier", Font.CENTER_BASELINE, 18);
    b4.setFont(font);
    b4.setBackground(Color.GREEN);
    b4.setBounds(260,280,165,50);
    f.add(b4);

    b4.addActionListener(
      new ActionListener() {
        public void actionPerformed(ActionEvent e)
        {
          System.exit(0);
        }
      }
    );

    f.setBackground(new Color(0xfff0f0f0));
    f.setTitle("CHOICES");
    f.setSize(500,500);
    f.setLayout(null);
    f.setVisible(true);

    f.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
          f.dispose();
      }
      });
  }
}

// Request for book

class Request{
  Font font;
  Request()
  {
    Frame f1=new Frame();
    f1.setTitle("Request for book");
    String[] BookList={"C++","Java","Python","C","DataStructure"};
    Label bookname=new Label("Enter the Book Name");
    font= new Font("Courier", Font.PLAIN, 17);
    bookname.setFont(font);
    bookname.setForeground(Color.BLUE);
    bookname.setBounds(30, 60, 160,40);
    f1.add(bookname);

    TextField bookname1=new TextField();
    font= new Font("Courier", Font.PLAIN, 15);
    bookname1.setFont(font);
    bookname1.setBounds(200, 60, 130,40);
    f1.add(bookname1);

    Button submit=new Button("Submit");
    font= new Font("Courier", Font.CENTER_BASELINE, 20);
    submit.setFont(font);
    submit.setBounds(150, 150, 100, 30);
    f1.add(submit);
    
    submit.addActionListener(
      new ActionListener() {
        public void actionPerformed(ActionEvent e)
        {
          int i;
          for(i=0;i<BookList.length;i++){
          if(bookname1.getText().equals(BookList[i]))
          {
            JOptionPane.showMessageDialog(f1,"Book Available");
            SimpleDateFormat formatter=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date=new Date();
	          String dt=formatter.format(date);
              JOptionPane.showMessageDialog(f1,"Date and Time of Book issue "+dt);
           
          
            break;
          }
          
        }
        if(i==BookList.length){
          JOptionPane.showMessageDialog(f1,"Book is not Available","Check",JOptionPane.ERROR_MESSAGE);
        }
        Label msg2=new Label(" Do You Want To Continue ");
        font= new Font("Courier", Font.CENTER_BASELINE, 19);
         msg2.setFont(font);
         msg2.setForeground(Color.BLUE);
            msg2.setBounds(80,230,250,30);
            f1.add(msg2);
           CheckboxGroup cbg=new CheckboxGroup();
           Checkbox yes=new Checkbox("YES",cbg,false);
           font= new Font("Courier", Font.BOLD, 18);
           yes.setFont(font);
           yes.setForeground(Color.BLUE);
           yes.setBounds(140, 280, 50, 30);
           f1.add(yes);
           yes.addItemListener(new ItemListener() {    
            public void itemStateChanged(ItemEvent e) {   
                new choice();   
                f1.dispose();
             }
          }
          
          );
           Checkbox no1=new Checkbox("NO",cbg,false);
           no1.setBounds(220, 280, 50, 30);
           font= new Font("Courier", Font.BOLD, 18);
           no1.setFont(font);
           no1.setForeground(Color.BLUE);
           f1.add(no1);
           no1.addItemListener(new ItemListener() {    
            public void itemStateChanged(ItemEvent e) {   
                f1.dispose();           
             }
          }
         );

        }
      }
    );
    

    f1.setSize(400,400);
    f1.setLayout(null);
    f1.setBackground(new Color(0xfff0f0f0));
    f1.setVisible(true);
    f1.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
          f1.dispose();
      }
      });
  }
}

//Book Returned

class Return 
{
  Font font;
  Return()
  {
    Frame f2=new Frame();
    String[] BookList={"C++","Java","Python","C","DataStructure"};

    Label rebookname=new Label("Enter the Book that you return");
    font= new Font("Courier", Font.PLAIN, 16);
    rebookname.setFont(font);
    rebookname.setBounds(30, 60, 220,40);
    f2.add(rebookname);

    TextField rebookname1=new TextField();
    font= new Font("Courier", Font.PLAIN, 15);
    rebookname1.setFont(font);
    rebookname1.setBounds(270, 60, 130,40);
    f2.add(rebookname1);

    Button return1=new Button("Return");
    font= new Font("Courier", Font.CENTER_BASELINE, 20);
    return1.setFont(font);
    return1.setBounds(150, 150, 110, 30);
    f2.add(return1);

    return1.addActionListener(
      new ActionListener() {
        public void actionPerformed(ActionEvent e)
        {
          int i;
          for(i=0;i<BookList.length;i++){
          if(rebookname1.getText().equals(BookList[i]))
          {
            JOptionPane.showMessageDialog(f2,"Book Returned");
            SimpleDateFormat formatter=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date=new Date();
	          String dt=formatter.format(date);
              JOptionPane.showMessageDialog(f2,"Date and Time of Book Return "+dt);
            break;
          }
          }
         if(i==BookList.length){
          JOptionPane.showMessageDialog(f2," This Book is not in Library ","Check",JOptionPane.ERROR_MESSAGE);
         }

          Label remsg2=new Label("Do you want to continue");
          font= new Font("Courier", Font.CENTER_BASELINE, 19);
          remsg2.setFont(font);
          remsg2.setBounds(80,230,250,30);
          f2.add(remsg2);

          CheckboxGroup cbg=new CheckboxGroup();
          Checkbox yes=new Checkbox("YES",cbg,false);
          font= new Font("Courier", Font.BOLD, 18);
          yes.setFont(font);
          yes.setBounds(140, 280, 50, 30);
          f2.add(yes);

          yes.addItemListener(new ItemListener() {    
          public void itemStateChanged(ItemEvent e) {   
            new choice();   
            f2.dispose();
              }
            }
          );

      Checkbox no1=new Checkbox("NO",cbg,false);
      no1.setBounds(220, 280, 50, 30);
      font= new Font("Courier", Font.BOLD, 18);
      no1.setFont(font);
      f2.add(no1);

      no1.addItemListener(new ItemListener() {    
      public void itemStateChanged(ItemEvent e) {   
            f2.dispose();           
              }
            }   
          );
        }
      }
    );

    f2.setSize(450,450);
    f2.setTitle("  Book Return ");
    f2.setLayout(null);
    f2.setVisible(true);
    f2.setBackground(new Color(0xfff0f0f0));
    f2.addWindowListener(new WindowAdapter() {
    public void windowClosing(WindowEvent we) {
          f2.dispose();
        }
     }
    );
  }
}

//View Books

class viewBooks{
  Font font;
  viewBooks()
  {
    Frame f3=new Frame();
    Label view=new Label(" Books Available Are ");
    font= new Font("Courier", Font.BOLD, 17);
    view.setFont(font);
    view.setBounds(90,30, 200, 30);
    f3.add(view);

    String[] BookList={"C++","Java","Python","C","DataStructure"};
    
    TextArea view1=new TextArea();
    font= new Font("Courier", Font.PLAIN, 17);
    view1.setFont(font);
    view1.setBounds(100,90,150,150);
    f3.add(view1);
    for(int i=0;i<BookList.length;i++){
      view1.append(BookList[i] + " \n");
    }

    Label remsg3=new Label("Do you want to continue");
    font= new Font("Courier", Font.CENTER_BASELINE, 19);
    remsg3.setFont(font);
    remsg3.setBounds(80,250,250,30);
    f3.add(remsg3);

    CheckboxGroup cbg=new CheckboxGroup();
    Checkbox yes=new Checkbox("YES",cbg,false);
    font= new Font("Courier", Font.BOLD, 18);
    yes.setFont(font);
    yes.setBounds(140, 300, 50, 30);
    f3.add(yes);

    yes.addItemListener(new ItemListener() {    
      public void itemStateChanged(ItemEvent e) {   
          new choice();   
            f3.dispose();
         }
       }
      );

    Checkbox no1=new Checkbox("NO",cbg,false);
    no1.setBounds(220, 300, 50, 30);
    font= new Font("Courier", Font.BOLD, 18);
    no1.setFont(font);
    f3.add(no1);

       no1.addItemListener(new ItemListener() {    
        public void itemStateChanged(ItemEvent e) {   
            f3.dispose();           
         }
      }   
     );
    
    f3.setSize(400,400);
    f3.setLayout(null);
    f3.setTitle(" View Books ");
    f3.setBackground(new Color(0xfff0f0f0));
    f3.setVisible(true);
    f3.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
          f3.dispose();
      }
      });
  }
}