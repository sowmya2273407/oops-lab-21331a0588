#include <iostream>
using namespace std;
class vehicle {
public:
    void Travel() {
        cout << "we can travel using vehicles" << endl;
    }
};

class Groundvehicle :virtual public vehicle {
public:
    void ground() {
        cout << "vehicles move in ground" << endl;
    }
};

class watervehicle :virtual public vehicle {
public:
    void water() {
        cout << "vehicles also movie in water" << endl;
    }
};
class car : public Groundvehicle, public watervehicle {
public:
    void display() {
        cout << "car move in water and ground" << endl;
    }
};

int main() {
    car obj;
    obj.Travel();   
    obj.ground();
    obj.water();
    obj.display();
}
