class Calculator
{
    int add(int x)
    {
        return ++x;
    }
    int add(int x,int y)
    {
        return x+y;
    }
    int add(int x,int y,int z)
    {
        return x+y+z;
    }
public static void main(String[] args) {
    
    Calculator obj=new Calculator();
    System.out.println("increment is "+obj.add(5));
    System.out.println("addition of two number is "+obj.add(4,5));
    System.out.println("addition of three numbers is "+obj.add(1,2,3));
}
}