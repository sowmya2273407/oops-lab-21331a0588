interface vehicle {
    default void Travel() {
        System.out.println("we can travel using vehicles");
    }
}
interface Groundvehicle extends vehicle {
    default void ground() {
        System.out.println("vehicles move in ground");
    }
}
interface watervehicle extends vehicle {
    default void water() {
        System.out.println("vehicles also movie in water");
    }
}
class car implements Groundvehicle,watervehicle {
    void display() {
        System.out.println("car move in water and ground");
    }
}
class Sample{
    public static void main(String[] args) {
        car obj=new car ();
        obj.Travel();   
        obj.ground();
        obj.water();
        obj.display();
    }
}
    

