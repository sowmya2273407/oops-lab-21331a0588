import java.awt.*;
import java.awt.event.*;
class Sampleui extends Frame {
  Sampleui()
  {
    Label titLabel=new Label("Login Form");
    add(titLabel);
    titLabel.setBounds(150,60,80, 20);
    titLabel.setBackground(Color.CYAN);

    Label uname=new Label("Enter your name");
    TextField uname1=new TextField();
    uname.setBounds(30,100,100,30);
     uname1.setBounds(150,100,80,30);

     Label pword=new Label("Registration Number");
    TextField pword1=new TextField();
    pword.setBounds(30,150,120,30);
     pword1.setBounds(150,150,80,30);

     Label lblGender=new Label("Gender");  
        lblGender.setBounds(30,190,80,30);
        add(lblGender);
       
        CheckboxGroup cbg = new CheckboxGroup();
       
        Checkbox checkMale = new Checkbox("Male",cbg,true);
        checkMale.setBounds(150,190, 80, 30);
        add(checkMale);
       
        Checkbox checkFemale = new Checkbox("Female",cbg,false);
        checkFemale.setBounds(230,190, 80, 30);
        add(checkFemale);

        Label branch=new Label("Branch");
        branch.setBounds(30, 220, 80, 30);
        add(branch);

        TextField branch1=new TextField();
        branch1.setBounds(150, 220, 80, 30);
        add(branch1);


    Button b=new Button("LOGIN");
    b.setBounds(90,300,80,30);
    b.setBackground(Color.GREEN);
 
    add(b);
    add(uname);
    add(uname1);
    add(pword);
    add(pword1);

    setSize(500,500);
   setBackground(Color.LIGHT_GRAY);
    setTitle("USER REGISTRATION FORM");
    setLayout(null);
    setVisible(true); 
    addWindowListener(new WindowAdapter()
     {
      public void windowClosing(WindowEvent e)
      {
        dispose();
      }
    });
     
  }
  
  public static void main(String[] args) {
    Sampleui obj=new Sampleui();
  }
  
}    