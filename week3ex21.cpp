#include <iostream>
using namespace std;
class Box
{
  private:
    float length1, width1, height1;
  public:
   void display(float length,float width,float height){
    length1=length;
    width1=width;
    height1=height;
  }
    friend void displayBoxDimensions(Box) ;
    void boxArea(float length, float width,float height) {  
        cout<<"area is :"<<2*(length*width+width*height+height*length)<<endl;
    }
    void boxVolume(float length, float width, float height);
    inline void displayWelcomeMessage() {
      cout<<"hi"<<endl;
    }
};

 
void Box :: boxVolume(float length, float width, float height)
{
    cout<<"volume is :"<<length*width*height<<endl;
}
void displayBoxDimensions(Box obj)
{
  cout<<"length:"<<obj.length1<<endl;
  cout<<"width:"<<obj.width1<<endl;
  cout<<"height:"<<obj.height1<<endl;
}
int main()
 {
  Box obj;
    float length;
    float width;
    float height;
    cout<<"Enter length: "; 
    cin>>length;
    cout<<"Enter width: ";
    cin>>width;
    cout<<"Enter height: "; 
    cin>>height;   
    obj.boxArea(length,width,height);
    obj.display(length,width,height);
    obj.boxVolume(length,width,height);
    displayBoxDimensions(obj);
    obj.displayWelcomeMessage();
 }
