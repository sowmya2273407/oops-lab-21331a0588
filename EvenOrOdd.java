import java.util.Scanner;
 class IsEvenOrOdd
 {
   static boolean isEven(int n){
    
     return n%2==0;
   }
   public static void main(String args[])
   { 
    Scanner num=new Scanner(System.in);
    int n;
    
    System.out.println("Enter number");
    n=num.nextInt();
    if(isEven(n))
    System.out.println(n+" is even number ");
    else
    System.out.println(n+" is odd number ");
   }
}