import java.util.HashMap;
import java.util.Map;

public class MapExample {
    public static void main(String[] args) {
        Map<String, Integer> studentScores = new HashMap<>();

        
        studentScores.put("Alice", 90);
        studentScores.put("Bob", 85);
        studentScores.put("Charlie", 95);
        studentScores.put("David", 80);

        
        int aliceScore = studentScores.get("Alice");
        System.out.println("Alice's score: " + aliceScore);

        
        boolean hasBob = studentScores.containsKey("Bob");
        System.out.println("Has Bob: " + hasBob);

        
        studentScores.put("Charlie", 98);

       
        studentScores.remove("David");
        System.out.println(studentScores.keySet());
        System.out.println(studentScores.values());
        for (Map.Entry<String, Integer> entry : studentScores.entrySet()) {
            String name = entry.getKey();
            int score = entry.getValue();
            System.out.println(name + "'s score: " + score);
        }

        int size = studentScores.size();
        System.out.println("Map size: " + size);

        
        studentScores.clear();

        
        boolean isEmpty = studentScores.isEmpty();
        System.out.println("Map is empty: " + isEmpty);
    }
}
