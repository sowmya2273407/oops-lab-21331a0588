import java.util.*;
public class Collectionsqueue {
    public static void main(String[] args) {
    Queue<String> queue = new LinkedList<>();
    queue.add("Apple");
    queue.add("Banana");
    queue.poll();
    System.out.println(queue);

    //array deque

    Queue<String> queue1 = new ArrayDeque<>();
    queue1.offer("Car");
    queue1.offer("Van");
    System.out.println(queue1);

    //deque
    Deque<String> deque = new ArrayDeque<String>();  
    deque.add("Gautam");  
    deque.add("Karan");  
    deque.add("Ajay");  
    //Traversing elements  
    for (String str : deque) {  
    System.out.println(str);  
    }  

    //Priority queue
    PriorityQueue<String> queue2=new PriorityQueue<String>();  
    queue2.add("Amit Sharma");  
    queue2.add("Vijay Raj");  
    queue2.add("JaiShankar");  
    queue2.add("Raj");  
    System.out.println("head:"+queue2.element());  
    System.out.println("head:"+queue2.peek());  
    System.out.println("iterating the queue elements:");  
    Iterator itr=queue2.iterator();  
    while(itr.hasNext()){  
    System.out.println(itr.next());  
    }  
    queue2.remove();  
    queue2.poll();  
    System.out.println("after removing two elements:");  
    Iterator<String> itr2=queue2.iterator();  
    while(itr2.hasNext()){  
    System.out.println(itr2.next());  
    }  


    }
    
}
